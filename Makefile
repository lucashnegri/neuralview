DESTDIR=/
PREFIX=/opt/neuralview

.PHONY: all
all:

.PHONY:
clean:

.PHONY: reference
reference:
	mkdir -p docs/reference
	luadoc --nofiles -i docs/indexr.html \
		view/*.lua core/*.lua *.lua addons/*.lua -d docs/reference

.PHONY: prepare_pkg
prepare_pkg: clean reference
	po/makepo.lua update all
	
.PHONY: install
install:
	mkdir -p $(DESTDIR)/$(PREFIX)
	cp -r docs view core addons examples po $(DESTDIR)/$(PREFIX)
	cp MainApp.lua Config.lua Globals.lua readme.md logo.png COPYING neuralview Makefile neuralview.desktop $(DESTDIR)/$(PREFIX)
	chmod +x $(DESTDIR)/$(PREFIX)/MainApp.lua
	chmod +x $(DESTDIR)/$(PREFIX)/neuralview

.PHONY: clean_pkg
clean_pkg:
	lua5.1 po/makepo.lua clean all
	rm -rf docs/reference
